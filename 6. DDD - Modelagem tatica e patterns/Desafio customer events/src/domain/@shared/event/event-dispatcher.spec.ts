import Customer from "../../customer/entity/customer";
import CustomerAddressChangedEvent from "../../customer/event/customer-address-changed.event";
import CustomerCreatedEvent from "../../customer/event/customer-created.event";
import EnviaConsoleLogHandler from "../../customer/event/handler/envia-consolelog-handler";
import EnviaConsoleLog1Handler from "../../customer/event/handler/envia-consolelog1-handler";
import EnviaConsoleLog2Handler from "../../customer/event/handler/envia-consolelog2-handler";
import Address from "../../customer/value-object/address";
import SendEmailWhenProductIsCreatedHandler from "../../product/event/handler/send-email-when-product-is-created.handler";
import ProductCreatedEvent from "../../product/event/product-created.event";
import EventDispatcher from "./event-dispatcher";

describe("Domain events tests", () => {
  
  it("should register an event handler", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();

    eventDispatcher.register("ProductCreatedEvent", eventHandler);

    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"]).toBeDefined();
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"].length).toBe(1);
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);
  });

  it("should register customer create event handler", () => {
    const dispatcher = new EventDispatcher();
    const handler1 = new EnviaConsoleLog1Handler();
    const handler2 = new EnviaConsoleLog2Handler();

    dispatcher.register("CustomerCreatedEvent", handler1);
    dispatcher.register("CustomerCreatedEvent", handler2);

    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"]).toBeDefined();
    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"].length).toBe(2);
    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"][0]).toMatchObject(handler1);
    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"][1]).toMatchObject(handler2);
  });

  it("should register customer change address event handler", () => {
    const dispatcher = new EventDispatcher();
    const handler = new EnviaConsoleLogHandler();    

    dispatcher.register("CustomerAddressChangedEvent", handler);    

    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"]).toBeDefined();
    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"].length).toBe(1);
    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"][0]).toMatchObject(handler);    
  });

  it("should unregister an event handler", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();

    eventDispatcher.register("ProductCreatedEvent", eventHandler);

    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);

    eventDispatcher.unregister("ProductCreatedEvent", eventHandler);

    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"]).toBeDefined();
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"].length).toBe(0);
  });

  it("should unregister customer create event handler", () => {
    const dispatcher = new EventDispatcher();
    const handler1 = new EnviaConsoleLog1Handler();
    const handler2 = new EnviaConsoleLog2Handler();

    dispatcher.register("CustomerCreatedEvent", handler1);
    dispatcher.register("CustomerCreatedEvent", handler2);

    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"][0]).toMatchObject(handler1);
    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"][1]).toMatchObject(handler2);

    dispatcher.unregister("CustomerCreatedEvent", handler1);
    dispatcher.unregister("CustomerCreatedEvent", handler2);

    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"]).toBeDefined();
    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"].length).toBe(0);
  });

  it("should unregister customer address changed event handler", () => {
    const dispatcher = new EventDispatcher();
    const handler = new EnviaConsoleLogHandler();

    dispatcher.register("CustomerAddressChangedEvent", handler);

    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"][0]).toMatchObject(handler);

    dispatcher.unregister("CustomerAddressChangedEvent", handler);

    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"]).toBeDefined();
    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"].length).toBe(0);
  });

  it("should unregister all event handlers", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();

    eventDispatcher.register("ProductCreatedEvent", eventHandler);
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);
    eventDispatcher.unregisterAll();
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"]).toBeUndefined();
  });

  it("should notify all product event handlers", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();
    const spyEventHandler = jest.spyOn(eventHandler, "handle");

    eventDispatcher.register("ProductCreatedEvent", eventHandler);

    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);
    const productCreatedEvent = new ProductCreatedEvent({
      name: "Product 1",
      description: "Product 1 description",
      price: 10.0,
    });
    
    eventDispatcher.notify(productCreatedEvent);
    expect(spyEventHandler).toHaveBeenCalled();
  });

  it("should notify all customer event handlers", () => {
    const dispatcher = new EventDispatcher();
    
    const customerCreatedHandler1 = new EnviaConsoleLog1Handler();
    const customerCreatedHandler2 = new EnviaConsoleLog2Handler();
    const customerAddressChangedHandler = new EnviaConsoleLogHandler();

    const spyEventHandler1 = jest.spyOn(customerCreatedHandler1, "handle");
    const spyEventHandler2 = jest.spyOn(customerCreatedHandler2, "handle");
    const spyEventHandler3 = jest.spyOn(customerAddressChangedHandler, "handle");

    dispatcher.register("CustomerCreatedEvent", customerCreatedHandler1);
    dispatcher.register("CustomerCreatedEvent", customerCreatedHandler2);
    dispatcher.register("CustomerAddressChangedEvent", customerAddressChangedHandler);       

    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"][0]).toMatchObject(customerCreatedHandler1);
    expect(dispatcher.getEventHandlers["CustomerCreatedEvent"][1]).toMatchObject(customerCreatedHandler2);
    expect(dispatcher.getEventHandlers["CustomerAddressChangedEvent"][0]).toMatchObject(customerAddressChangedHandler);
    
    const cliente = new Customer("1", "Customer 1"); 
    const customerCreatedEvent = new CustomerCreatedEvent({ name: cliente.name, });
       
    const endereco = new Address("Street 1", 123, "13330-250", "São Paulo");
    cliente.Address = endereco;
    cliente.changeAddress(endereco);

    const customerAddressChangedEvent = new CustomerAddressChangedEvent(cliente);
    
    dispatcher.notify(customerCreatedEvent);
    expect(spyEventHandler1).toHaveBeenCalled();
    expect(spyEventHandler2).toHaveBeenCalled();

    dispatcher.notify(customerAddressChangedEvent);
    expect(spyEventHandler3).toHaveBeenCalled();
  });
});
