import EventHandlerInterface from "../../../@shared/event/event-handler.interface";
import Customer from "../../entity/customer";
import CustomerAddressChangedEvent from "../customer-address-changed.event";

export default class EnviaConsoleLogHandler
  implements EventHandlerInterface<CustomerAddressChangedEvent>
{
  handle(event: CustomerAddressChangedEvent): void {
    var cliente = event.eventData as Customer;
    console.log(`O endereço do cliente ${cliente.id + ' - ' + cliente.name} foi alterado para: 
    ${cliente.Address.toString()}`);
  }
}