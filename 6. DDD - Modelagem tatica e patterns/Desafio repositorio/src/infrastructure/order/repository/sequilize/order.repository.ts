import Order from "../../../../domain/checkout/entity/order";
import OrderItem from "../../../../domain/checkout/entity/order-item";
import OrderRepositoryInterface from "../../../../domain/checkout/repository/order-repository.interface";
import OrderItemModel from "./order-item.model";
import OrderModel from "./order.model";

export default class OrderRepository implements OrderRepositoryInterface {

  /**
   * Salva o pedido informado no banco
   * @param entity: pedido a ser salvo no banco
   */
  async create(entity: Order): Promise<void> {
    await OrderModel.create(
      {
        id: entity.id,
        customer_id: entity.customerId,
        total: entity.total(),
        items: entity.items.map((item) => ({
          id: item.id,
          name: item.name,
          price: item.price,
          product_id: item.productId,
          quantity: item.quantity,
        })),
      },
      {
        include: [{ model: OrderItemModel }],
      }
    );
  }

  /**
   * Atualiza o pedido informado
   * @param entity: pedido a ser atualizado    
   */
  async update(entity: Order): Promise<void> {
    await OrderModel.update(
      {
        id: entity.id,
        customer_id: entity.customerId,
        total: entity.total(),
        items: entity.items.map((item) => ({
          id: item.id,
          name: item.name,
          price: item.price,
          product_id: item.productId,
          quantity: item.quantity,
        })),
      },
      {
        where: {
          id: entity.id,
        },
      }
    );
  }

  /**
   * Localiza o pedido com o id informado
   @param id: identificador do pedido a ser localizado
   */
   async find(id: string): Promise<Order> {
    let orderModel;
    try {
      orderModel = await OrderModel.findOne({
        where: { id, },         
        include: [{ model: OrderItemModel }],
        rejectOnEmpty: true
      });
    } catch (error) {
      throw new Error("Order not found");
    }

    return new Order(orderModel.id,
      orderModel.customer_id,
      orderModel.items.map(orderItemModel => new OrderItem(
          orderItemModel.id,
          orderItemModel.name,
          orderItemModel.price,
          orderItemModel.product_id,
          orderItemModel.quantity)));
  }

  /**
   * Lista todos os pedidos
   */
   async findAll(): Promise<Order[]> {
    const orders = await OrderModel.findAll({ include: [{ model: OrderItemModel }] })    
    return orders.map(order => {
      const orderItems: Array<OrderItem> = []

      for (const item of order.items) {
        orderItems.push(new OrderItem(item.id, item.name, item.price, item.product_id, item.quantity))
      }

      return new Order(order.id, order.customer_id, orderItems)
    });
  }

}
