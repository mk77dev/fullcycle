import { Sequelize } from 'sequelize-typescript';
import Product from '../../domain/entity/product';
import ProductModel from '../db/sequelize/model/product.model';
import ProductRepository from './product.repository';

describe("Teste do repositório de produto", () => {
    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: 'sqlite',
            storage: ':memory:',
            logging: false,
            sync: { force: true },
        });
        sequelize.addModels([ProductModel]);
        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it("deve ser possível criar um produto", async () => {
        const repositorio = new ProductRepository();
        const produto = new Product("1", "Produto 1", 100);

        await repositorio.create(produto);

        const productModel = await ProductModel.findOne({ where: { id: "1" } });

        expect(productModel.toJSON()).toStrictEqual({
            id: "1",
            name: "Produto 1",
            price: 100,
        });
    });

    it("deve ser possível atualizar um produto", async () => {        
        const repositorio = new ProductRepository();
        const produto = new Product("1", "Produto1", 100);

        await repositorio.create(produto);

        const productModel = await ProductModel.findOne({ where: { id: "1" } });
        expect(productModel.toJSON()).toStrictEqual({
            id: "1",
            name: "Produto1",
            price: 100,
        });

        produto.changeName("Produto2");
        produto.changePrice(99);

        await repositorio.update(produto);

        const productModel2 = await ProductModel.findOne({ where: { id: "1" } });
        expect(productModel2.toJSON()).toStrictEqual({
            id: "1",
            name: "Produto2",
            price: 99,
        });
    });

    it("deve ser possível localizar um produto", async () => {
        
        const repositorio = new ProductRepository();
        const produto = new Product("1", "Produto1", 100);

        await repositorio.create(produto);

        const productModel = await ProductModel.findOne({ where: { id: "1" } });

        const produtoLocalizado = await repositorio.find("1");

        expect(productModel.toJSON()).toStrictEqual({
            id: produtoLocalizado.id,
            name: produtoLocalizado.name,
            price: produtoLocalizado.price,
        });
    });

    it("deve ser possível localizar todos os produto", async () => {
        
        const repositorio = new ProductRepository();
        const produto = new Product("1", "Produto1", 100);
        await repositorio.create(produto);

        const produto2 = new Product("2", "Produto2", 99);
        await repositorio.create(produto2);

        const produtosLocalizados = await repositorio.findAll();
        const produtos = [produto,produto2];
        
        expect(produtos).toEqual(produtosLocalizados);
    });

});