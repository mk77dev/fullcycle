import Product from "../entity/product";
import ProductService from "./product.service";

describe("Testes unitários do serviço de produto", () => {
    it("deve ser possível mudar o preço de todos os produtos", () => {
        const produto1 = new Product("123", "produto muito legal", 5);
        const produto2 = new Product("456", "outro produto manero", 10);
        const produtos = [produto1,produto2];
        ProductService.increasePrice(produtos, 100);
        expect(produto1.price).toBe(10);
        expect(produto2.price).toBe(20);
    });
});
