import Customer from "../entity/customer";
import Order from "../entity/order";
import OrderItem from "../entity/order-item";
import OrderService from "./order.service";

describe("Testes unitários do serviço de domínio de pedido", () => {

    it("Deve ser possível criar um novo pedido", () => {
        const cliente = new Customer("1", "Zé");
        const item = new OrderItem("idItem", "item muito legal", 10, "produto1", 1);
        const pedido = OrderService.placeOrder(cliente, [item]);

        expect(cliente.rewardPoints).toBe(5);
        expect(pedido.total()).toBe(10);
    });

    it("Deve ser possível obter o total de todos os pedidos", () => {
        const item1 = new OrderItem("item1", "item muito legal", 10, "produto1", 1);
        const item2 = new OrderItem("item2", "item muito legal", 20, "produto2", 1);

        const pedido1 = new Order("pedido1", "cliente1", [item1]);
        const pedido2 = new Order("pedido2", "cliente2", [item2]);

        const total = OrderService.total([pedido1, pedido2]);

        expect(total).toBe(30);
    });
})