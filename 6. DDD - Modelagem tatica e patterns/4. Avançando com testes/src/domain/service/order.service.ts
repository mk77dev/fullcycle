import Customer from "../entity/customer";
import Order from "../entity/order";
import OrderItem from "../entity/order-item";
import {v4 as uuid} from "uuid";

export default class OrderService {
    
    static placeOrder(cliente: Customer, itens: OrderItem[]) : Order {

        if(itens.length === 0) {
            throw new Error("O pedido deve ter pelo menos um item");
        }

        const pedido = new Order(uuid(), cliente.id, itens);
        cliente.addRewardPoints(pedido.total()/2);
        return pedido;
    }

    static total(pedidos: Order[]): number {
        return pedidos.reduce((acc,order) => acc + order.total(), 0);
    }
}