import OrderItem from "./order-item";

export default class Order {
    private _id: string;
    private _customerId: string;
    private _items: OrderItem[];
    private _total: number;

    constructor(id: string, customerId: string, items: OrderItem[]) {
        this._id = id;
        this._customerId = customerId;
        this._items = items;
        this._total = this.total();
        this.validate();
    }

    get id(): string {
        return this._id;
    }

    get customerId(): string {
        return this._customerId;
    }
    
    get items(): OrderItem[] {
        return this._items;
    }

    validate(): boolean {
        if(this._id.length === 0) {
            throw new Error("O id é obrigatório");
        }

        if(this._customerId.length === 0) {
            throw new Error("O id do cliente é obrigatório");
        }

        if(this._items.length === 0) {
            throw new Error("O pedido deve conter pelo menos um item");
        }

        if(this._items.some(item => item.quantity <= 0)){
            throw new Error("A quantidade deve ser maior que 0");
        }

        return true;
    }

    total(): number {
        return this._items.reduce((acc, item) => acc + item.price, 0)
    }
}