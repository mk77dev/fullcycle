import Address from "./address";
import Customer from "./customer";

describe("Testes unitários de customer", () => {

    it("deve lançar erro se o id for vazio", () => {
        expect(() => {
            let cliente = new Customer("","Fei");
        }).toThrowError("O id é obrigatório");
    });

    it("deve lançar erro se o id for vazio", () => {
        expect(() => {
            let cliente = new Customer("1","");
        }).toThrowError("O nome é obrigatório");
    });

    it("deve ser possível mudar o nome do cliente", () => {
        let cliente = new Customer("890","João");
        cliente.changeName("Zé");
        expect(cliente.name).toBe("Zé");
    });

    it("deve ser possível ativar o cliente", () => {
        let cliente = new Customer("890","Cliente muito manero");
        let endereco = new Address("Rua Dois", 2, "02002-002", "São Tomé das Letras");
        cliente.Address = endereco;
        cliente.activate();
        expect(cliente.isActive()).toBe(true);
    });

    it("deve ser possível desativar o cliente", () => {
        let cliente = new Customer("890","Cliente muito manero");        
        cliente.deactivate();
        expect(cliente.isActive()).toBe(false);
    });

    it("deve lançar erro ao ativar um cliente sem endereço", () => {
        expect(()=> {
            var cliente = new Customer("891","Cliente muito manero");
            cliente.activate();
        }).toThrowError("Para ativar o cliente é necessário um endereço");
    });

    it("deve ser possível adicionar reward points", () => {

        const cliente = new Customer("1", "Cliente");
        expect(cliente.rewardPoints).toBe(0);

        cliente.addRewardPoints(10);
        expect(cliente.rewardPoints).toBe(10);

        cliente.addRewardPoints(10);
        expect(cliente.rewardPoints).toBe(20);
    });

});