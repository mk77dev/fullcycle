import OrderItem from "./order-item";
import Order from "./order";

describe("Testes unitários de pedido", () => {

    it("deve lançar erro se o id for vazio", () => {
        expect(() => {
            let pedido = new Order("","123",[]);
        }).toThrowError("O id é obrigatório");
    });

    it("deve lançar erro se o id do cliente for vazio", () => {
        expect(() => {
            let pedido = new Order("123","",[]);
        }).toThrowError("O id do cliente é obrigatório");
    });
    
    it("deve lançar erro se o não houver nenhum item", () => {
        expect(() => {
            let pedido = new Order("123","123",[]);
        }).toThrowError("O pedido deve conter pelo menos um item");
    });

    it("deve lançar erro se a quantidade de itens é maior que 0", () => {        

        expect(() => {
            const item = new OrderItem("123", "item muito legal", 100, "p1", 0);
            const pedido = new Order("pedido1", "cliente1", [item]);
        }).toThrowError("A quantidade deve ser maior que 0");
    });

});