import Product from "./product";

describe("Testes unitários de produto" , () => {

    it("Deve retornar erro se o id for vazio", () => {
        expect(() => {
            const produto = new Product("", "Produto 1", 100);
        }).toThrowError("O id é obrigatório");
    });

    it("Deve retornar erro se o nome for vazio", () => {
        expect(() => {
            const produto = new Product("1", "", 100);
        }).toThrowError("O nome é obrigatório");
    });

    it("Deve retornar erro se o preço for maior que zero", () => {
        expect(() => {
            const produto = new Product("1", "Produto 1", -1);
        }).toThrowError("Preço deve ser maior que zero");
    });

    it("Deve ser possível mudar o nome do produto", () => {
        
        const produto = new Product("1", "Produto 1", 100);
        produto.changeName("Produto 2");
        expect(produto.name).toBe("Produto 2");
    });

    it("Deve ser possível mudar o preço do produto", () => {
        
        const produto = new Product("1", "Produto 1", 100);
        produto.changePrice(95);
        expect(produto.price).toBe(95);
    });

});