import Address from "./domain/entity/address";
import Customer from "./domain/entity/customer";
import Order from "./domain/entity/order";
import OrderItem from "./domain/entity/order-item";

let cliente = new Customer("456", "José do Egito");
const endereco = new Address("Rua Hum", 1, "01001-001", "Vila Inhocunhé");
cliente.Address = endereco;
cliente.activate();

const item1 = new OrderItem("1", "item muito legal", 5, "produto1", 99);
const item2 = new OrderItem("", "outro item muito manero..", 10, "produto2", 49);

const pedido = new Order("1", "678", [item1, item2]);