import ProductFactory from "../../../domain/product/factory/product.factory";
import UpdateProductUseCase from "./update.product.usecase";

const product = ProductFactory.create("a", "Bicicleta", 999);

const input = {
  id: product.id,
  name: "Bicicleta atualizada",
  price: 1120
};

const MockRepository = () => {
  return {
    create: jest.fn(),
    findAll: jest.fn(),
    find: jest.fn().mockReturnValue(Promise.resolve(product)),
    update: jest.fn(),
  };
};

describe("Unit test for product update use case", () => {
  it("should update a product", async () => {
    const repository = MockRepository();
    const customerUpdateUseCase = new UpdateProductUseCase(repository);

    const output = await customerUpdateUseCase.execute(input);

    expect(output).toEqual(input);
  });
});