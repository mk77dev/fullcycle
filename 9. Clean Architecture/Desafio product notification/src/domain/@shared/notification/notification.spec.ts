import Notification from "./notification";

describe("Unit tests for notifications", () => {
  it("should create errors", () => {
    const notification = new Notification();
    
    const customerError = {
      message: "error message",
      context: "customer",
    };
    notification.addError(customerError);
    expect(notification.messages("customer")).toBe("customer: error message,");

    const productError = {
      message: "error message",
      context: "product",
    };
    notification.addError(productError);
    expect(notification.messages("product")).toBe("product: error message,");

    const orderError = {
      message: "error message",
      context: "order",
    };
    notification.addError(orderError);
    expect(notification.messages("order")).toBe("order: error message,");

    expect(notification.messages()).toBe("customer: error message,product: error message,order: error message,");
  });

  it("should check if notification has at least two product errors", () => {
    const notification = new Notification();

    const erroProduto1 = {
      message: "mensagem de erro do produto 1",
      context: "product",
    };
    notification.addError(erroProduto1);

    const erroProduto2 = {
      message: "mensagem de erro do produto 2",
      context: "product",
    };
    notification.addError(erroProduto2);

    expect(notification.messages()).toBe("product: mensagem de erro do produto 1,product: mensagem de erro do produto 2,");
  });

  it("should check if notification has at least one error", () => {
    const notification = new Notification();
    const error = {
      message: "error message",
      context: "customer",
    };
    notification.addError(error);

    expect(notification.hasErrors()).toBe(true);
  });

  it("should get all errors props", () => {
    const notification = new Notification();
    const error = {
      message: "error message",
      context: "customer",
    };
    notification.addError(error);

    expect(notification.getErrors()).toEqual([error]);
  });
});
