import CreateProductUseCase from "./create.product.usecase";
const input = {
  type: "a",
  name: "Chapeu",
  price: 100
};

const MockRepository = () => {
  return {
    find: jest.fn(),
    findAll: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
  };
};

describe("Unit test create product use case", () => {
  it("should create a product", async () => {
    const repository = MockRepository();
    const productCreateUseCase = new CreateProductUseCase(repository);

    const output = await productCreateUseCase.execute(input);

    expect(output).toEqual({
      id: expect.any(String),
      name: input.name,
      price: input.price      
    });
  });

  it("should thrown an error when name is missing", async () => {
    const repository = MockRepository();
    const productCreateUseCase = new CreateProductUseCase(repository);

    input.name = "";

    await expect(productCreateUseCase.execute(input)).rejects.toThrow(
      "product: Name is required"
    );
  });

  it("should thrown an error when price is less than or equal zero", async () => {
    const repository = MockRepository();
    const productCreateUseCase = new CreateProductUseCase(repository);

    input.price = -1;

    await expect(productCreateUseCase.execute(input)).rejects.toThrow(
      "product: price must be a positive number"
    );
  });
});