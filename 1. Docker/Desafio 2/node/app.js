var express = require('express')
var app = express()

app.set('view engine', 'ejs')

const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database:'nodedb'
}

const mysql = require('mysql')
const connection = mysql.createConnection(config)

app.use(express.json())
app.use(express.urlencoded({
extended: true}))

app.get('/', async (req, res) => {

const ddl = 'CREATE TABLE IF NOT EXISTS people (id int not null auto_increment, name varchar(50), primary key(id)) ENGINE=InnoDB'
connection.query(ddl)
connection.query(`INSERT INTO people(name) values ('Wesley')`)
connection.query('SELECT * FROM people', function (err, result) {
res.render("pessoas", { userData: result }) })
})

app.listen(3000)